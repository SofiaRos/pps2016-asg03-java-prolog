package u07asg;


import javax.swing.*;
import java.awt.*;
import java.util.Optional;

/** A not so nicely engineered App to run TTT games, not very important.
 * It just works..
 */
public class TicTacToeApp {

    private final TicTacToe ttt;
    private final JButton[][] board = new JButton[3][3];
    private final JButton exit = new JButton("Exit");
    private final JFrame frame = new JFrame("TTT");
    private boolean finished = false;
    private Player turn = Player.PlayerX;
    private int moves = 0;

    private void changeTurn(){
        this.turn = this.turn == Player.PlayerX ? Player.PlayerO : Player.PlayerX;
    }

    public TicTacToeApp(TicTacToe ttt) throws Exception {
        this.ttt=ttt;
        initPane();
    }
    
    private void humanMove(int i, int j){
        if (ttt.move(turn,i,j)){
            board[i][j].setText(turn == Player.PlayerX ? "X" : "O");
            moves++;
            changeTurn();
            if (moves > 3){
                System.out.println("X winning count: "+ttt.winCount(turn, Player.PlayerX));
                System.out.println("O winning count: "+ttt.winCount(turn, Player.PlayerO));
            }
        }
        Optional<Player> victory = ttt.checkVictory();
        if (victory.isPresent()){
            exit.setText(victory.get()+" won!");
            finished=true;
            return;
        }
        if (ttt.checkCompleted()){
            exit.setText("Even!");
            finished=true;
            return;
        }
    }

    private void initPane(){
        frame.setLayout(new BorderLayout());
        JPanel b=new JPanel(new GridLayout(3,3));
        for (int i=0;i<3;i++){
            for (int j=0;j<3;j++){
                final int i2 = i;
                final int j2 = j;
                board[i][j]=new JButton("");
                b.add(board[i][j]);
                board[i][j].addActionListener(e -> { if (!finished) humanMove(i2,j2); });
            }
        }
        JPanel s=new JPanel(new FlowLayout());
        s.add(exit);
        exit.addActionListener(e -> System.exit(0));
        frame.add(BorderLayout.CENTER,b);
        frame.add(BorderLayout.SOUTH,s);
        frame.setSize(200,230);
        frame.setVisible(true);
    }

    public static void main(String[] args){
        try {
            new TicTacToeApp(new TicTacToeImpl("src/u07asg/ttt.pl"));
        } catch (Exception e) {
            System.out.println("Problems loading the theory");
        }
    }
}
